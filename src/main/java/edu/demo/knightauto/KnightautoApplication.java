package edu.demo.knightauto;

import edu.demo.knightauto.knights.Hero;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class KnightautoApplication {

	private final Hero knight;

	public static void main(String[] args) {
		SpringApplication.run(KnightautoApplication.class, args);
	}

	@Autowired
	public KnightautoApplication(Hero knight) {
		this.knight= knight;
	}

	@PostConstruct
	private void start() {
		knight.embarkOnMission();
	}

}
