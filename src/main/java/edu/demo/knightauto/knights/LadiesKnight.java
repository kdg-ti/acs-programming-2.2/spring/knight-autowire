package edu.demo.knightauto.knights;


public class LadiesKnight implements Hero {
	private RescueDamselQuest quest;

	public LadiesKnight() {
		this.quest = new RescueDamselQuest();
	}

	public void embarkOnMission() {
		quest.embark();
	}
}
