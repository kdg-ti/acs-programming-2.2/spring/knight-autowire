package edu.demo.knightauto.knights;


public interface Mission {
    void embark();
}
