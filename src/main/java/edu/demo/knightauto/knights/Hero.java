package edu.demo.knightauto.knights;


public interface Hero {
    void embarkOnMission();
}
