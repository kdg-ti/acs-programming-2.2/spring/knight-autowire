package edu.demo.knightauto.knights;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class HandyKnight implements Hero {
	private final Mission quest;

	@Autowired
	public HandyKnight(Mission quest) {
		this.quest = quest;
	}

	@Override
	public void embarkOnMission() {
		quest.embark();
	}
}


